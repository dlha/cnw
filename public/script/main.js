$('.carousel').carousel({
    pause: false,
    interval: 5000
});

// $(document).ready(function() {
//     $('.dropdown-toggle').hover(
//         function (){
//             $(this).dropdown('show');
//             // $(this).children('.dropdown-menu').css('display: block');
//         },
//         function (){
//             $(this).dropdown('hide').trigger('click');
//         },
//         function (){
//             $(this).dropdown('hide').trigger('mouseleave');
//         },
//     );
// });


$(window).scroll(function () {
    if ($(window).scrollTop() >= 39) {
        $('nav').addClass('shadow-sm');
    }
    else {
        $('nav').removeClass('shadow-sm');
    }
});

// Config lightbox
lightbox.option({
    albumLabel: "Hình ảnh %1 trên %2",
    wrapAround: true,
})


// Load map over ajax

function loadMap() {
    var num = new google.maps.LatLng(15.982474199999999,108.2496261);
  var mapProp= {
    center: num, mapTypeId: google.maps.MapTypeId.HYBRID,
    zoom:16,
  };
  var maps = new google.maps.Map(document.getElementById("googleMap"),mapProp);
  
  var marker = new google.maps.Marker({ position: num, map: maps, title:"Trụ sở công ty Travel.com"});
  }

$(document).ready(function() {
   $('#callBtn').click(function(){
        $('#location').load("callMap.txt", function () {
            loadMap();
        });
   }) ;
});


// Auto dropdown on hover 


